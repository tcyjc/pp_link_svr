import os


class FifoBase:

    def __init__(self, base_path):
        self.fifo_dir = base_path + "\\cache\\fifo\\"
        self.read_path = self.fifo_dir + "read.fi"
        self.write_path = self.fifo_dir + "read.fo"
        if os.path.exists(self.read_path):
            os.remove(self.read_path)
        if os.path.exists(self.write_path):
            os.remove(self.write_path)
        os.mkfifo(self.write_path)
        os.mkfifo(self.read_path)
        self.rf = os.open(self.read_path, os.O_RDONLY)
        self.wf = os.open(self.write_path, os.O_SYNC | os.O_CREAT | os.O_RDWR)

    def write(self, val):
        os.write(self.wf, val)

    def read(self, size_val):
        os.read(self.rf, size_val)

    def close(self):
        os.close(self.rf)
        os.close(self.wf)

