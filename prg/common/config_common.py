import yaml
from prg.common.common_base import CommonBase
from prg.entity.config_entity import *


class ConfigBase:
    config_items = {}

    def __init__(self, path, en=None, root=None):
        self.base_en = en
        self.path = path
        self.config_dict = None
        self.config = None
        self.root = root
        self.read_config()

    def read_config(self):
        with open(self.path, mode="r") as fp:
            self.config_dict = yaml.load(stream=fp, Loader=yaml.FullLoader)
        if self.root is None:
            self.config = CommonBase.dict_to_entity(dict_item=self.config_dict, en=self.base_en)
        else:
            self.config = CommonBase.dict_to_entity(dict_item=self.config_dict[self.root], en=self.base_en)

    @staticmethod
    def get_config(name, path=None, en=None, root=None):
        if name not in ConfigBase.config_items and path is not None:
            ConfigBase.config_items[name] = ConfigBase(path=path, en=en, root=root)
        return ConfigBase.config_items[name]

