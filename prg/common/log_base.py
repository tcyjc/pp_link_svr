import logging


def get_logging(path, level):
    logger_obj = logging.getLogger()
    fh = logging.FileHandler(path)
    fh.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    format_item = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(format_item)
    ch.setFormatter(format_item)
    return logger_obj


log_message = get_logging("message.log")
log_beat = get_logging("beat.log")
