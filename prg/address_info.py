class AddressInfo:

    def __init__(self, ip=None, port=None):
        self.ip = ip
        self.port = port

    def bind_val(self, ip, port):
        self.ip = ip
        self.port = port

    def get_ip(self):
        return self.ip

    def get_port(self):
        return self.port

    def set_ip(self, ip):
        self.ip = ip

    def set_port(self, port):
        self.port = port
