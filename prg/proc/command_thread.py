import time
from prg.common.object_message import MessageBase
from prg.entity.public_entity import PublicEntity


class RunCommand(MessageBase):

    def __init__(self, name="command"):
        super().__init__(name=name)
        self.command_queue = PublicEntity.instance().command

    def add(self, cmd):
        self.command_queue.put(cmd)

    def get_queue(self):
        return self.command_queue

    def entry(self):
        while self.is_run():
            try:
                if not self.command_queue.empty():
                    cmd = self.command_queue.get()
                    cmd.send()
                    time.sleep(0.5)
            except Exception as e:
                print("command error: %s" % e)
