import time
from prg.common.object_message import MessageBase
from prg.command_info import CommandInfo
from prg.entity.public_entity import PublicEntity


class KeepAlive(MessageBase):

    def __init__(self, name="command"):
        super().__init__(name=name)
        self.keep_list = {}
        self.queue_items = PublicEntity.instance().command

    def add(self, key, val):
        self.keep_list[key] = val

    def entry(self):
        while self.is_run():
            for key, val in self.keep_list.items():
                cmd = CommandInfo(socket_item=val.get_command_socket(), command="beep", name=key, arg=None)
                self.queue_items.put(cmd)
            time.sleep(10)


