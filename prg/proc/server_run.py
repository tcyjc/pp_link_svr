from prg.header_info import HeaderInfo
from prg.entity.public_entity import PublicEntity
from prg.common.object_message import MessageBase


class RunServer(MessageBase):
    def __init__(self, name="server", socket_item=None):
        super().__init__(name=name)
        self.client_socket = socket_item
        self.handle = PublicEntity.instance().handle

    def entry(self):
        with self.client_socket as cli_socket:
            while self.is_run():
                try:
                    header_inf = HeaderInfo(client_socket=cli_socket)
                    self.handle.put(header_inf)
                except Exception as e:
                    print("server error %s" % e)
                    break

