from prg.header_info import HeaderInfo
from prg.command_info import CommandInfo
import socket


class ServerCommand:
    def __init__(self):
        self.server_socket = None

    def bind(self, socket_item):
        self.server_socket = socket_item

    def connect(self, parent_address):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.connect(parent_address)

    def send(self,  command, arg, callback=None):
        command_item = CommandInfo(socket_item=self.server_socket, name="svr_cmd", command=command, arg=arg)
        with self.server_socket as socket_item:
            command_item.send()
            if callback is not None:
                header_inf = HeaderInfo(client_socket=socket_item)
                header_en = header_inf.get_head_entity()
                if header_en.command == command and header_en.arg.sign == arg.sign:
                    callback(header_en)

