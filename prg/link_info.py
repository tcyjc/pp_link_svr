from prg.address_info import AddressInfo
from prg.entity.address_entity import AddressEntity
from prg.common.common_base import CommonBase
from threading import Thread
from prg.entity.authority_entity import *
import time


class LinkItem(AddressInfo):
    def __init__(self, name, socket_item):
        super().__init__()
        self.__bind_socket__(socket_item)
        self.name = name
        self.verification = CommonBase.random_code(size=32)
        self.sign = CommonBase.random_code(size=16)
        self.socket_item = socket_item
        # self.udp_port = udp_port
        self.status = False
        self.udp_addr = None
        self.user_info = None
        self.timeout = 0
        self.timeout_thread = None
        self.link_id = None

    def set_link_id(self, link_id):
        self.link_id = link_id

    def get_link_id(self):
        return self.link_id

    def set_timeout(self, sec):
        self.timeout = int(time.time()) + sec
        self.timeout_thread = Thread(target=self.thread_timeout)

    def set_user_info(self, user_inf):
        self.user_info = user_inf

    def get_user_info(self):
        return self.user_info

    def set_udp_addr(self, addr):
        self.udp_addr = addr

    def get_udp_addr(self):
        return self.udp_addr

    def get_sign(self):
        return self.sign

    def set_status(self, status):
        self.status = status

    def get_status(self):
        return self.status

    # def get_udp_port(self):
    #     return self.udp_port

    def get_verification(self):
        return self.verification

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_socket(self):
        return self.socket_item

    def thread_timeout(self):
        time.sleep(self.timeout)

    def __bind_socket__(self, socket_item):
        ip_port = socket_item.getpeername()
        self.bind_val(ip_port[0], ip_port[1])

    def get_cli_address(self):
        addr = AddressEntity()
        addr.ip = self.get_ip()
        addr.port = self.get_port()
        return addr

    # def set_udp_process(self, udp, port):
    #     self.udp_process = udp
    #     self.udp_port = port
    #
    # def get_udp_process(self):
    #     return self.udp_process, self.udp_port
