from prg.header_info import HeaderInfo
from prg.common.common_base import CommonBase


class CommandInfo:
    def __init__(self, socket_item, name, command, arg=None):
        self.socket_item = socket_item
        self.name = name
        self.command = command
        self.arg = arg

    def get_socket(self):
        return self.socket_item

    def get_name(self):
        return self.name

    def get_arg(self):
        return self.arg

    def send(self):
        cmd_item = HeaderInfo()
        head_entity = cmd_item.bind_header(name=self.name, command=self.command, arg=self.arg)
        json_str = CommonBase.entity_to_json(head_entity)
        send_byte = bytes(json_str, encoding='utf-8')
        data_len = len(send_byte)
        data_len_bytes = data_len.to_bytes(4, byteorder='big', signed=False)
        self.socket_item.send(data_len_bytes)
        send_index = 0
        while send_index < data_len:
            self.socket_item.send(send_byte[send_index:send_index+1024])
            send_index += 1024

