from prg.pass_link_svr import LinkSvr
from prg.bll.config_bll import ConfigBll
from prg.bll.authority_bll import AuthorityBll
# from prg.common.config_common import ConfigBase
# from prg.entity.config_entity import ConfigEntity
# from prg.common.common_base import CommonBase
# from prg.header_info import HeadEntity
# import queue

if __name__ == "__main__":
    AuthorityBll.instance(path="../authority.yaml")
    ConfigBll.instance(path="../config.yaml")
    run_val = LinkSvr()
    run_val.open_listen()
