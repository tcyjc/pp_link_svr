from prg.common.config_common import ConfigBase
from prg.entity.config_entity import ConfigEntity
from threading import Lock
from queue import Queue


class ConfigBll:
    _instance_lock = Lock()

    def __init__(self, path):
        self.config = ConfigBase.get_config(name="config", path=path, en=ConfigEntity(), root="config")
        # self.search_list = []
        # with open(self.get_server().search, mode="r") as fp:
        #     self.search_list = fp.readlines()

    def get_base_config(self):
        return self.config.config

    def get_host(self):
        return self.get_base_config().host

    def get_udp(self):
        return self.get_base_config().udp

    def get_port(self):
        return self.get_host().port

    def get_ip(self):
        return self.get_host().ip

    def get_server(self):
        return self.get_base_config().server

    # def get_search_list(self):
    #     return self.search_list

    # def get_udp_port(self):
    #     return self.get_base_config().udp.start, self.get_base_config().udp.end

    def get_udp_port(self, queue_item=None):
        if queue_item is None:
            queue_item = Queue()
        for num in range(self.get_base_config().udp.start, self.get_base_config().udp.end):
            queue_item.put(num)
        return queue_item

    @staticmethod
    def instance(path=None):
        if not hasattr(ConfigBll, "_instance"):
            with ConfigBll._instance_lock:
                if not hasattr(ConfigBll, "_instance"):
                    ConfigBll._instance = ConfigBll(path)
        return ConfigBll._instance
