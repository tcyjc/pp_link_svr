from threading import Lock
from prg.bll.config_bll import ConfigBll


class UdpBll:
    _instance_lock = Lock()

    def __init__(self):
        self.port_queue = ConfigBll.instance().get_udp_port()

    def get_port(self):
        assert not self.port_queue.empty()
        return self.port_queue.get()

    def back_port(self, port):
        self.port_queue.put(port)

    @staticmethod
    def instance():
        if not hasattr(UdpBll, "_instance"):
            with UdpBll._instance_lock:
                if not hasattr(UdpBll, "_instance"):
                    UdpBll._instance = UdpBll()
        return UdpBll._instance

