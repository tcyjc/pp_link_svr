class HostEntity:
    pass


class AuthoritySvrEntity:
    pass


class UdpEntity:
    pass


class TimeoutEntity:
    pass


class ServerEntity:
    pass


class ConfigEntity:
    timeout = TimeoutEntity()
    host = HostEntity()
    authority = AuthoritySvrEntity()
    udp = UdpEntity()
    server = ServerEntity()
