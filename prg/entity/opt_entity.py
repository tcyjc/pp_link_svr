class OptInfo:
    addr_name = None
    link_status = False
    addr = None
    port = None
    aut_item = None


class OptEntity:
    name = None
    command = None
    status = False
    initiator = OptInfo()
    target = OptInfo()

