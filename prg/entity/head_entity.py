from prg.entity.arg_entity import ArgBase


class HeadEntity:
    command = None
    name = None
    arg = ArgBase()
