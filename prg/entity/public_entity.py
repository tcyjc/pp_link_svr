from threading import Lock
from queue import Queue


class PublicEntity:
    _instance_lock = Lock()

    def __init__(self):
        self.command = Queue()
        self.handle = Queue()
        self.port = Queue()
        self.addr = dict()
        self.udp = dict()
        self.server_id = 0
        self.server_number = 0
        self.client_number = 0

    @staticmethod
    def instance():
        if not hasattr(PublicEntity, "_instance"):
            with PublicEntity._instance_lock:
                if not hasattr(PublicEntity, "_instance"):
                    PublicEntity._instance = PublicEntity()
        return PublicEntity._instance

