# pp_link_svr

#### 介绍
点对点连接工具服务端
一个点对点连接工具，n年前C#写的，失业无聊在家改成了python加了点功能，主要功能是使用udp进行穿透，穿透成功后将udp端口的数据转发到本地对应tcp对应端口，同时监听本地tcp端口，将tcp端口数据通过udp端口转发到目标机器（然后再由目标机器将udp端口数据转发到它的本地tcp端口中）。

#### 软件架构


#### 安装教程
1、直接运行__main__.py

#### 使用说明
配置文件<br/>
config.yaml<br/>
config:<br/>
&nbsp;&nbsp;server：服务配置（暂时无效）<br/>
&nbsp;&nbsp;&nbsp;&nbsp;parent：上级服务器地址（当本地没有找到需要连接的对象时会向上级服务器询问）<br/>
&nbsp;&nbsp;&nbsp;&nbsp;search：可以用来搜索的服务器地址列表<br/>
&nbsp;&nbsp;&nbsp;&nbsp;bind：用来监听其他服务器询问的地址端口<br/>
&nbsp;&nbsp;timeout：注册用户超时配置<br/>
&nbsp;&nbsp;guest：游客连接超时配置<br/>
&nbsp;&nbsp;host：服务器配置<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ip：服务器监听地址<br/>
&nbsp;&nbsp;&nbsp;&nbsp;port：监听端口<br/>
&nbsp;&nbsp;udp：服务端udp端口范围<br/>
&nbsp;&nbsp;&nbsp;&nbsp;start：开始<br/>
&nbsp;&nbsp;&nbsp;&nbsp;end：结束<br/>
	<br/>
authority.yaml<br/>
user：用户列表<br/>
&nbsp;&nbsp;name：用户名<br/>
&nbsp;&nbsp;password：用户密码（废弃但请勿删除）<br/>
&nbsp;&nbsp;aes：验证码，用来在请求连接前验证请求是否合法<br/>

#### 参与贡献



#### 码云特技

